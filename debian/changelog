hspell-gui (0.2.6-5) unstable; urgency=low

  [ Tzafrir Cohen ]
  * Add myself as uploader.
  * Standards version 3.9.1 (no change needed).
  * Depend on ${misc:Depends}.
  * Switch from cdbs to dh.
  * Convert source changes to patches:
    - Patch applet: Missing hspell-applet.server.
    - Patch he_po: Extra Hebrew translations.
    - Patch autoconf-gnomeui: Also depend on libgnomeui-2.0.

  [ Lior Kaplan ]
  * Switch to dpkg-source 3.0 (quilt) format

 -- Lior Kaplan <kaplan@debian.org>  Thu, 07 Apr 2011 01:27:15 +0300

hspell-gui (0.2.6-4.1) unstable; urgency=low

  * Non-maintainer upload.
  * Detect pkg-config package libgnomeui-2.0 and use its flags.
    Re-generate ./configure. Fix FTBFS. (Closes: #537033)
  * debian/control: add build dependency on libgnomeui-dev

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 06 Sep 2009 15:24:14 +0200

hspell-gui (0.2.6-4) unstable; urgency=low

  * Fix FTBFS if built twice in a row (Closes: #442600)
    - Thank you Lucas & Patrick for testing / reporting these issues.
  * debian/control: move Homepage field from the description to the newly 
    supported header.

 -- Lior Kaplan <kaplan@debian.org>  Sat, 27 Oct 2007 11:20:38 +0200

hspell-gui (0.2.6-3) unstable; urgency=low

  * Update menu for the new menu policy, s/Apps/Applications/

 -- Baruch Even <baruch@debian.org>  Thu, 05 Jul 2007 09:04:47 +0300

hspell-gui (0.2.6-2) unstable; urgency=low

  [ Baruch Even ]
  * Update watch file

  [ Lior Kaplan ]
  * Update standards-version to 3.7.2 (no changes needed)

 -- Lior Kaplan <kaplan@debian.org>  Sun,  9 Jul 2006 20:11:48 +0300

hspell-gui (0.2.6-1) unstable; urgency=low

  * New upstream release
    + Added swedish translation (Closes: #348204)

 -- Baruch Even <baruch@debian.org>  Sun, 15 Jan 2006 22:02:57 +0000

hspell-gui (0.2.5-1) unstable; urgency=low

  * New upstream version (Closes: #310610)
  * Change maintainership to the Debian-Hebrew group.
  * Add Lior Kaplan to uploaders.
  * Change standards-version to 3.6.2, no changes needed.

 -- Baruch Even <baruch@debian.org>  Thu, 28 Jul 2005 13:14:12 +0100

hspell-gui (0.2.4-1ubuntu) unstable; urgency=low

  * Fix locale problem in ubuntu.

 -- Yaacov Zamir <kzamir@walla.co.il>  Thu, 09 Jun 2005 22:34:29 +0000

hspell-gui (0.2.3-1ubuntu) unstable; urgency=low

  * Fix text entry fucos problem

 -- Yaacov Zamir <kzamir@walla.co.il>  Fri, 03 Jun 2005 22:34:29 +0000

hspell-gui (0.2.2-2) unstable; urgency=low

  * Update priority to optional since that's what the ftp-masters made it.

 -- Baruch Even <baruch@debian.org>  Thu, 17 Mar 2005 22:34:29 +0000

hspell-gui (0.2.2-1) unstable; urgency=low

  * New upstream version.
  * Add watch file.

 -- Baruch Even <baruch@debian.org>  Wed, 06 Oct 2004 23:02:52 +0100

hspell-gui (0.2.1-1) unstable; urgency=low

  * New upstream version.

 -- Baruch Even <baruch@debian.org>  Sat, 22 May 2004 10:13:47 +0300

hspell-gui (0.2-3) unstable; urgency=low

  * Change priority to extra since we depend on hspell which is in extra.

 -- Baruch Even <baruch@debian.org>  Sun, 16 May 2004 22:13:33 +0300

hspell-gui (0.2-2) unstable; urgency=low

  * Missed build-dependency on debhelper, was sure that cdbs did that already.
    (Closes: #248450)

 -- Baruch Even <baruch@debian.org>  Tue, 11 May 2004 21:00:26 +0300

hspell-gui (0.2-1) unstable; urgency=low

  * Initial Release.

 -- Baruch Even <baruch@debian.org>  Sat,  3 Apr 2004 00:00:00 +0200

